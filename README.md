fwait
=====

A command-line tool for waiting for file system changes.

Usage
-----

`$ fwait [file_or_directory] ...`

`fwait` without arguments is equivalent to `fwait .`.

Examples
--------

Wait for all the files and folders in the current
directory to change once. Then do something:

```
$ fwait; echo 'Something changed'
```

Wait and execute an action over and over:

```
$ while fwait; do echo 'Something changed'; done
```

Installation
------------

Run `./install.js` as a super user.
