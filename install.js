#!/usr/bin/env node

const fs = require('fs')

const target = '/usr/bin/fwait'

try {
    fs.unlinkSync(target)
} catch (e) {
    const code = e.code
    if (code === 'EACCES') {
        console.log('ERROR: Permission denied')
        process.exit(1)
    }
    if (e.code !== 'ENOENT') throw e
}

try {
    fs.symlinkSync(__dirname + '/index.js', target)
} catch (e) {
    if (e.code === 'EACCES') {
        console.log('ERROR: Permission denied')
        process.exit(1)
    }
    throw e
}
