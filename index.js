#!/usr/bin/env node

const files = process.argv.slice(2)
if (files.length === 0) files.push('.')
require('./lib/Watch')(files)
