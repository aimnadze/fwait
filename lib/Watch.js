const fs = require('fs')

module.exports = paths => {

    function scan (path) {
        watch(path)
        if (!fs.statSync(path).isDirectory()) return
        fs.readdirSync(path).forEach(item => {
            scan(path + '/' + item)
        })
    }

    function watch (filename) {
        fs.watch(filename, () => {
            clearTimeout(timeout)
            timeout = setTimeout(() => {
                console.log((new Date).toISOString(), 'INFO: Changed')
                process.exit()
            }, 100)
        })
    }

    let timeout
    paths.forEach(scan)

}
